package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Contacto;

import javax.swing.JLabel;

public class Agenda extends JFrame {

	private JPanel contentPane;
	private JLabel lblAgenda;
	private ArrayList<Contacto> vContactos;
	private JLabel lblContactos;
	private JLabel lblInformacin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Agenda frame = new Agenda();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Agenda() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 522, 379);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblAgenda = new JLabel("Agenda 2.0");
		lblAgenda.setBounds(200, 23, 96, 15);
		contentPane.add(lblAgenda);
		
		lblContactos = new JLabel("Contactos");
		lblContactos.setBounds(58, 63, 96, 15);
		contentPane.add(lblContactos);
		
		lblInformacin = new JLabel("Información");
		lblInformacin.setBounds(303, 63, 103, 15);
		contentPane.add(lblInformacin);
	}
}
